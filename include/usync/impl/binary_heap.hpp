#pragma once

#include <functional>
#include <type_traits>
#include <utility>

namespace usync::impl {

    // Stolen from: https://github.com/libuv/libuv/blob/v1.x/src/heap-inl.h

    template <typename T, typename Comparer = std::less<T>>
    class BinaryHeap;

    class IntrusiveHeapNode {
        template <typename T, typename Comparer>
        friend class BinaryHeap;

    private:
        IntrusiveHeapNode *m_left;
        IntrusiveHeapNode *m_right;
        IntrusiveHeapNode *m_parent;

    public:
        constexpr IntrusiveHeapNode() noexcept
            : m_left(nullptr), m_right(nullptr), m_parent(nullptr) {}
    };

    template <class Self>
    class IntrusiveHeapBaseNode : public IntrusiveHeapNode {};

    template <typename T, typename Comparer>
    class BinaryHeap {
    private:
        IntrusiveHeapNode *m_root;
        size_t m_elements;
        Comparer m_comp;

    public:
        using value_type      = T;
        using pointer         = value_type*;
        using reference       = value_type&;
        using const_reference = const value_type&;

    private:
        static constexpr IntrusiveHeapNode &node(reference ref) noexcept {
            return static_cast<IntrusiveHeapNode&>(static_cast<IntrusiveHeapBaseNode<T>&>(ref));
        }

        static constexpr const_reference parent(const IntrusiveHeapNode &node) noexcept {
            return static_cast<const T&>(static_cast<const IntrusiveHeapBaseNode<T>&>(node));
        }

        static constexpr reference parent(IntrusiveHeapNode &node) noexcept {
            return static_cast<T&>(static_cast<IntrusiveHeapBaseNode<T>&>(node));
        }

    public:
        constexpr const_reference peek() const noexcept {
            return this->parent(*m_root);
        }

        constexpr reference peek() noexcept {
            return this->parent(*m_root);
        }

        constexpr size_t size() const noexcept {
            return m_elements;
        }

        constexpr bool is_empty() const noexcept {
            return m_elements == 0;
        }

    private:
        constexpr void swap_nodes(IntrusiveHeapNode *parent, IntrusiveHeapNode *child) noexcept {
            IntrusiveHeapNode *sibling;

            std::swap(*parent, *child);

            parent->m_parent = child;
            if (child->m_left == child) {
                child->m_left = parent;
                sibling = child->m_right;
            } else {
                child->m_right = parent;
                sibling = child->m_left;
            }

            if (sibling != nullptr) {
                sibling->m_parent = child;
            }

            if (parent->m_left != nullptr) {
                parent->m_left->m_parent = parent;
            }
            if (parent->m_right != nullptr) {
                parent->m_right->m_parent = parent;
            }

            if (child->m_parent == nullptr) {
                m_root = child;
            } else if (child->m_parent->m_left == parent) {
                child->m_parent->m_left = child;
            } else {
                child->m_parent->m_right = child;
            }
        }

    public:
        constexpr void push(reference ref) noexcept {
            IntrusiveHeapNode **parent;
            IntrusiveHeapNode **child;
            unsigned int path;
            unsigned int n;
            unsigned int k;

            IntrusiveHeapNode *node = std::addressof(this->node(ref));
            node->m_left = nullptr;
            node->m_right = nullptr;
            node->m_parent = nullptr;

            // Calculate the path from the root to the insertion point. This is a min
            // heap, so we always insert at the left-most free node of the bottom row.
            path = 0;
            for (k = 0, n = 1 + m_elements; n >= 2; ++k, n >>= 1) {
                path = (path << 1) | (n & 1);
            }

            // Now traverse the heap using the path we calculated.
            parent = child = std::addressof(m_root);
            while (k > 0) {
                parent = child;
                if (path & 1) {
                    child = std::addressof((*child)->m_right);
                } else {
                    child = std::addressof((*child)->m_left);
                }

                path >>= 1;
                --k;
            }

            // Insert the new node.
            node->m_parent = *parent;
            *child = node;
            ++m_elements;

            // Walk up the tree and check at each node if the heap property holds.
            // It's a min heap so parent < child must be true.
            while (node->m_parent != nullptr && m_comp(ref, this->parent(*node->m_parent))) {
                this->swap_nodes(node->m_parent, node);
            }
        }

    private:
        constexpr void remove(IntrusiveHeapNode *node) noexcept {
            IntrusiveHeapNode *smallest;
            IntrusiveHeapNode **max;
            IntrusiveHeapNode *child;
            unsigned int path;
            unsigned int k;
            size_t n;

            if (m_elements == 0) {
                return;
            }

            // Calculate the path from the min (the root) to the max, the
            // left-most node of the bottom row.
            path = 0;
            for (k = 0, n = m_elements; n >= 2; ++k, n >>= 1) {
                path = (path << 1) | (n & 1);
            }

            // Now traverse the heap using the path we calculated.
            max = std::addressof(m_root);
            while (k > 0) {
                if (path & 1) {
                    max = std::addressof((*max)->m_right);
                } else {
                    max = std::addressof((*max)->m_left);
                }

                path >>= 1;
                --k;
            }

            --m_elements;

            // Unlink the max node.
            child = *max;
            *max = nullptr;

            if (child == node) {
                // We're removing either the max or the last node in the tree.
                if (child == m_root) {
                    m_root = nullptr;
                }
                return;
            }

            // Replace the to be deleted node with the max node.
            child->m_left = node->m_left;
            child->m_right = node->m_right;
            child->m_parent = node->m_parent;

            if (child->m_left != nullptr) {
                child->m_left->m_parent = child;
            }

            if (child->m_right != nullptr) {
                child->m_right->m_parent = child;
            }

            if (node->m_parent == nullptr) {
                m_root = child;
            } else if (node->m_parent->m_left == node) {
                node->m_parent->m_left = child;
            } else {
                node->m_parent->m_right = child;
            }

            // Walk down the subtree and check at each node if the heap property holds.
            // It's a min heap so parent < child must be true. If the parent is bigger,
            // swap it with the smallest child.
            for (;;) {
                smallest = child;
                if (child->m_left != nullptr && m_comp(this->parent(*child->m_left), this->parent(*smallest))) {
                    smallest = child->m_left;
                }
                if (child->m_right != nullptr && m_comp(this->parent(*child->m_right), this->parent(*smallest))) {
                    smallest = child->m_right;
                }
                if (smallest == child) {
                    break;
                }
                this->swap_nodes(child, smallest);
            }

            // Walk up the subtree and check that each parent is less than the node
            // this is required, because `max` node is not guaranteed to be the
            // actual maximum in tree.
            while (child->m_parent != nullptr && m_comp(this->parent(*child), this->parent(*child->m_parent))) {
                this->swap_nodes(child->m_parent, child);
            }
        }

    public:
        constexpr void remove(reference ref) noexcept {
            this->remove(std::addressof(this->node(ref)));
        }

        constexpr void pop() noexcept {
            this->remove(m_root);
        }
    };

}
