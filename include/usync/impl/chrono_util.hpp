#pragma once

#include <algorithm>
#include <chrono>

#include <linux/time_types.h>

namespace usync::impl {

    template <typename R, typename P>
    constexpr __kernel_timespec duration_to_timespec(const std::chrono::duration<R, P> &d) noexcept {
        constexpr auto MinSecs  = std::chrono::seconds{0};
        constexpr auto MinNanos = std::chrono::nanoseconds{0};
        constexpr auto MaxNanos = std::chrono::nanoseconds{999'999'999};

        auto secs  = std::max(std::chrono::duration_cast<std::chrono::seconds>(d), MinSecs);
        auto nanos = std::clamp(std::chrono::duration_cast<std::chrono::nanoseconds>(d - secs), MinNanos, MaxNanos);

        return {secs.count(), nanos.count()};
    }

}
