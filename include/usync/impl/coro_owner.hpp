#pragma once

#include <coroutine>
#include <utility>

namespace usync::impl {

    struct CoroutineOwner {
    protected:
        std::coroutine_handle<> m_handle;

    public:
        constexpr explicit CoroutineOwner(std::coroutine_handle<> h) noexcept
            : m_handle(h) {}

        ~CoroutineOwner() {
            if (m_handle != nullptr) {
                m_handle.destroy();
            }
        }

        // A coroutine cannot have multiple owners, so forbid copying.
        // Otherwise, this would lead to double frees.
        CoroutineOwner(const CoroutineOwner &) = delete;
        CoroutineOwner &operator=(const CoroutineOwner &) = delete;

        CoroutineOwner(CoroutineOwner &&rhs) noexcept
            : m_handle(std::exchange(rhs.m_handle, {})) {}

        CoroutineOwner &operator=(CoroutineOwner &&rhs) noexcept {
            std::swap(m_handle, rhs.m_handle);
            return *this;
        }
    };

}
