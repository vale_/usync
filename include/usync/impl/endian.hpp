#pragma once

#include <array>
#include <bit>
#include <cstdint>
#include <concepts>

namespace usync::impl {

    constexpr __uint128_t u128(uint64_t a, uint64_t b) noexcept {
        return (static_cast<__uint128_t>(a) << sizeof(uint64_t)) | static_cast<__uint128_t>(b);
    }

    template <typename T> requires (std::unsigned_integral<T> || std::is_same_v<T, __uint128_t>)
    constexpr T swap_bytes(T value) noexcept {
        if constexpr (std::is_same_v<T, uint8_t>) {
            return value;
        } else if constexpr (std::is_same_v<T, uint16_t>) {
            return __builtin_bswap16(value);
        } else if constexpr (std::is_same_v<T, uint32_t>) {
            return __builtin_bswap32(value);
        } else if constexpr (std::is_same_v<T, uint64_t>) {
            return __builtin_bswap64(value);
        } else if constexpr (std::is_same_v<T, __uint128_t>) {
            #if __has_builtin(__builtin_bswap128)
            return __builtin_bswap128(value);
            #else
            return (__builtin_bswap64(value >> 64) | (static_cast<T>(__builtin_bswap64(value)) << 64));
            #endif
        } else {
            static_assert(!std::is_same_v<T, T>, "unsupported type");
        }
    }

    template <typename T, std::size_t N>
    constexpr void swap_bytes_array(std::array<T, N> &value) noexcept {
        for (std::size_t i = 0; i < N; ++i) {
            value[i] = swap_bytes<T>(value[i]);
        }
    }

    template <typename T>
    constexpr T to_big_endian(T value) noexcept {
        if constexpr (std::endian::native == std::endian::little) {
            return swap_bytes<T>(value);
        } else {
            return value;
        }
    }

    template <typename T>
    constexpr T to_little_endian(T value) noexcept {
        if constexpr (std::endian::native == std::endian::little) {
            return value;
        } else {
            return swap_bytes<T>(value);
        }
    }

    template <typename T, std::size_t N>
    constexpr void to_big_endian_array(std::array<T, N> &value) noexcept {
        if constexpr (std::endian::native == std::endian::little) {
            swap_bytes_array(value);
        }
    }

    template <typename T, std::size_t N>
    constexpr void to_little_endian_array(std::array<T, N> &value) noexcept {
        if constexpr (std::endian::native == std::endian::big) {
            swap_bytes_array(value);
        }
    }

    template <typename T, typename... Args> requires (std::is_same_v<T, Args> && ...)
    constexpr auto to_big_endian_bytes(Args ...args) noexcept {
        std::array<T, sizeof...(Args)> raw{{to_big_endian<Args>(args)...}};
        return std::bit_cast<std::array<uint8_t, (sizeof(Args) + ...)>>(raw);
    }

    template <typename T, typename... Args> requires (std::is_same_v<T, Args> && ...)
    constexpr auto to_little_endian_bytes(Args ...args) noexcept {
        std::array<T, sizeof...(Args)> raw{{to_little_endian<Args>(args)...}};
        return std::bit_cast<std::array<uint8_t, (sizeof(Args) + ...)>>(raw);
    }

}
