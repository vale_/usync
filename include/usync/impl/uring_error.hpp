#pragma once

namespace usync::impl {

    [[gnu::cold]] void throw_io_uring_error(int res, const char *func);

}
