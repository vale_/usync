#pragma once

#include "usync/net/addr/ip_addr.hpp"
#include "usync/net/addr/parser.hpp"

namespace usync::net {

    /// An IPv4 socket address.
    ///
    /// IPv4 socket addresses consist of an IPv4 address and a 16-bit port number,
    /// as stated in IETF RFC 793.
    struct SocketAddrV4 {
        Ipv4Addr addr;
        uint16_t port;
    };

    /// An IPv6 socket address.
    ///
    /// IPv6 socket addresses consist of an IPv6 address, a 16-bit port number, as
    /// well as fields containing the traffic class, the flow label, and a scope
    /// identifier, as stated in IETF RFC 2553, Section 3.3.
    struct SocketAddrV6 {
        Ipv6Addr addr;
        uint16_t port;
        uint32_t flowinfo;
        uint32_t scope_id;
    };

    /// An internet socket address, either IPv4 or IPv6.
    struct SocketAddr {
    public:
        union {
            SocketAddrV4 v4;
            SocketAddrV6 v6;
        };

    private:
        bool m_is_v4;

    public:
        constexpr explicit SocketAddr(SocketAddrV4 a) noexcept
            : v4(a), m_is_v4(true) {}

        constexpr explicit SocketAddr(SocketAddrV6 a) noexcept
            : v6(a), m_is_v4(false) {}

        constexpr SocketAddr(Ipv4Addr addr, uint16_t port) noexcept
            : v4{addr, port}, m_is_v4(true) {}

        constexpr SocketAddr(Ipv6Addr addr, uint16_t port) noexcept
            : v6{addr, port, 0, 0}, m_is_v4(false) {}

    public:
        /// Indicates whether this is an IPv4 address.
        constexpr bool is_v4() const noexcept {
            return m_is_v4;
        }

        /// Indicates whether this is an IPv6 address.
        constexpr bool is_v6() const noexcept {
            return !m_is_v4;
        }

    public:
        /// Parses a socket address from the given string representation.
        static constexpr SocketAddr parse(std::string_view in) noexcept {
            constexpr int Tag_Ipv4 = 0;
            constexpr int Tag_Ipv6 = 1;

            struct ParseState {
                int active;
                union {
                    std::array<uint8_t, 4> v4;
                    struct {
                        std::array<uint16_t, 8> addr;
                        uint32_t scope_id;
                    } v6;
                };
                uint16_t port;

                constexpr ParseState() : active(Tag_Ipv4), v4(), port(0) {}
                constexpr ~ParseState() = default;
            } state;
            impl::IpAddressParser parser{in};

            int res = parser.parse_full([&]() {
                if (parser.parse_ipv4_addr(state.v4) != -1 && parser.parse_port(&state.port) != -1) {
                    return 0;
                } else {
                    std::construct_at(&state.v6, std::array<uint16_t, 8>{}, 0);
                    state.active = Tag_Ipv6;

                    if (parser.match('[')
                        && parser.parse_ipv6_addr(state.v6.addr) != -1
                        && (parser.parse_scope_id(&state.v6.scope_id) || true)
                        && parser.match(']')
                        && parser.parse_port(&state.port) != -1) {
                        return 0;
                    }
                }

                return -1;
            });

            if (res != -1) {
                if (state.active == Tag_Ipv4) {
                    return {Ipv4Addr{state.v4}, state.port};
                } else {
                    return SocketAddr({Ipv6Addr{state.v6.addr}, state.port, 0, state.v6.scope_id});
                }
            }

            // TODO: How error.
            return {ipv4::Localhost, 1337};
        }
    };

}
