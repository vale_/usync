#pragma once

#include <array>
#include <optional>

#include <netinet/in.h>

#include "usync/impl/endian.hpp"

namespace usync::net {

    /// An IPv4 address.
    ///
    /// IPv4 addresses are defined as 32-bit integers in IETF RFC 791.
    /// They are usually represented as four octets.
    struct Ipv4Addr;

    /// An IPv6 address.
    ///
    /// IPv6 addresses are defined as 128-bit integers in IETF RFC 4291.
    /// They are usually represented as eight 16-bit segments.
    struct Ipv6Addr;

    struct Ipv4Addr {
    private:
        std::array<uint8_t, 4> m_octets;

    public:
        /// The size of an IPv4 address in bits.
        static constexpr size_t bits = 32;

    public:
        /// Constructs an IPv4 address from four octets.
        constexpr Ipv4Addr(uint8_t a, uint8_t b, uint8_t c, uint8_t d) noexcept
            : m_octets({a, b, c, d}) {}

        /// Constructs an IPv4 address from an array holding the octets.
        constexpr explicit Ipv4Addr(const std::array<uint8_t, 4> &in) noexcept
            : m_octets{in} {}

    public:
        /// Creates an IPv4 address from its 32-bit value representation.
        static constexpr Ipv4Addr from_bits(uint32_t value) noexcept {
            auto octets = std::bit_cast<std::array<uint8_t, 4>>(::usync::impl::to_big_endian<uint32_t>(value));
            return {octets[0], octets[1], octets[2], octets[3]};
        }

        /// Converts the IPv4 address into a 32-bit value representation.
        constexpr uint32_t as_bits() const noexcept {
            return ::usync::impl::to_big_endian<uint32_t>(std::bit_cast<uint32_t>(m_octets));
        }

        /// Gets the four octets that make up this address.
        constexpr std::array<uint8_t, 4> octets() const noexcept {
            return m_octets;
        }

        /// Returns true for the special 'unspecified' address (`0.0.0.0`).
        constexpr bool is_unspecified() const noexcept {
            return this->as_bits() == 0;
        }

        /// Returns true if this is a loopback address (`127.0.0.0/8`).
        constexpr bool is_loopback() const noexcept {
            return m_octets[0] == 127;
        }

        /// Returns true if this is a private address.
        ///
        /// The private address ranges are defined in IETF RFC 1918 and include:
        ///
        /// - `10.0.0.0/9`
        /// - `172.16.0.0/12`
        /// - `192.168.0.0/16`
        constexpr bool is_private() const noexcept {
            if (m_octets[0] == 10) {
                return true;
            }
            if (m_octets[0] == 172 && (15 < m_octets[1] && m_octets[1] < 32)) {
                return true;
            }
            if (m_octets[0] == 192 && m_octets[1] == 168) {
                return true;
            }
            return false;
        }

        /// Returns true if the address is link-local (`169.254.0.0/16`).
        constexpr bool is_link_local() const noexcept {
            return m_octets[0] == 169 && m_octets[1] == 254;
        }

        /// Returns true if this address is part of the Shared Address Space.
        constexpr bool is_shared() const noexcept {
            return m_octets[0] == 100 && ((m_octets[1] & 0b1100'0000) == 0b0100'0000);
        }

        /// Returns true if this address is part of the `198.18.0.0/15` range.
        constexpr bool is_benchmarking() const noexcept {
            return m_octets[0] == 198 && (m_octets[1] & 0xFE) == 18;
        }

        /// Returns true if this is a multicast address (`224.0.0.0/4`).
        constexpr bool is_multicast() const noexcept {
            return m_octets[0] >= 224 && m_octets[0] <= 239;
        }

        /// Returns true if this is a broadcast address (`255.255.255.255`).
        constexpr bool is_broadcast() const noexcept {
            return this->as_bits() == 0xFFFF'FFFFU;
        }

        /// Returns true if this address is in a range designated for documentation.
        constexpr bool is_documentation() const noexcept {
            if (m_octets[0] == 192 && m_octets[1] == 0 && m_octets[2] == 2) {
                return true;
            }
            if (m_octets[0] == 198 && m_octets[1] == 51 && m_octets[2] == 100) {
                return true;
            }
            if (m_octets[0] == 203 && m_octets[1] == 0 && m_octets[2] == 113) {
                return true;
            }
            return false;
        }

        /// Returns true if this address is reserved by IANA for future use.
        constexpr bool is_reserved() const noexcept {
            return ((m_octets[0] & 240) == 240) && !this->is_broadcast();
        }

        /// Returns true if the address appears to be globally reachable.
        constexpr bool is_global() const noexcept {
            return !(m_octets[0] == 0 // "This network"
                || this->is_private()
                || this->is_shared()
                || this->is_loopback()
                || this->is_link_local()
                // Addresses reserved for future protocols (`192.0.0.0/24`);
                // .9 and .10 are documented as globally reachable so excluded.
                || (
                    m_octets[0] == 192 && m_octets[1] == 0 && m_octets[2] == 0 &&
                    m_octets[3] != 9 && m_octets[3] != 10
                )
                || this->is_documentation()
                || this->is_benchmarking()
                || this->is_reserved()
                || this->is_broadcast());
        }

        /// Converts this address to an IPv4-compatible IPv6 address.
        constexpr Ipv6Addr to_ipv6_compatible() const noexcept;

        /// Converts this address to an IPv4-mapped IPv6 address.
        constexpr Ipv6Addr to_ipv6_mapped() const noexcept;

        /// Converts this address to an `in_addr` instance.
        constexpr in_addr to_in_addr() const noexcept {
            // NOTE: We already store the address as BE internally on all machines.
            // A simple bit_cast will do to ensure the bytes are never swapped.
            return { .s_addr = std::bit_cast<in_addr_t>(m_octets) };
        }
    };

    struct Ipv6Addr {
    private:
        std::array<uint8_t, 16> m_octets;

    public:
        /// The size of an IPv6 address in bits.
        static constexpr size_t bits = 128;

    public:
        /// Constructs an IPv6 address from eight 16-bit segments.
        constexpr Ipv6Addr(uint16_t a, uint16_t b, uint16_t c, uint16_t d, uint16_t e, uint16_t f, uint16_t g, uint16_t h) noexcept
            : m_octets(::usync::impl::to_big_endian_bytes<uint16_t>(a, b, c, d, e, f, g, h)) {}

        /// Constructs an IPv4 address from an array holding the octets.
        constexpr explicit Ipv6Addr(const std::array<uint16_t, 8> &in) noexcept
            : Ipv6Addr(in[0], in[1], in[2], in[3], in[4], in[5], in[6], in[7]) {}

        /// Constructs an IPv6 address from sixteen octets.
        constexpr explicit Ipv6Addr(const std::array<uint8_t, 16> &octets) noexcept
            : m_octets{octets} {}

    public:
        /// Creates an IPv6 address from its 128-bit value representation.
        static constexpr Ipv6Addr from_bits(__uint128_t value) noexcept {
            auto segs = std::bit_cast<std::array<uint16_t, 8>>(::usync::impl::to_big_endian<__uint128_t>(value));
            return {segs[0], segs[1], segs[2], segs[3], segs[4], segs[5], segs[6], segs[7]};
        }

        /// Converts the IPv6 address into a 128-bit value representation.
        constexpr __uint128_t as_bits() const noexcept {
            return std::bit_cast<__uint128_t>(m_octets);
        }

        /// Gets the sixteen octets that make up this address.
        constexpr std::array<uint8_t, 16> octets() const noexcept {
            return m_octets;
        }

        /// Returns the eight 16-bit segments that make up this address.
        constexpr std::array<uint16_t, 8> segments() const noexcept {
            auto segs = std::bit_cast<std::array<uint16_t, 8>>(m_octets);
            ::usync::impl::to_big_endian_array(segs);
            return segs;
        }

        /// Returns true for the special 'unspecified' address (`::`).
        constexpr bool is_unspecified() const noexcept {
            return this->as_bits() == static_cast<__uint128_t>(0);
        }

        constexpr bool is_loopback() const noexcept {
            return this->as_bits() == static_cast<__uint128_t>(1);
        }

        /// Returns true if this is a unique local address (`fc00::/7`).
        constexpr bool is_unique_local() const noexcept {
            return (this->segments()[0] & 0xFE00) == 0xFC00;
        }

        /// Returns true if this is a multicast address (`ff00::/8`).
        constexpr bool is_multicast() const noexcept {
            return (this->segments()[0] & 0xFF00) == 0xFF00;
        }

        /// Returns true if this is a unicast address, as defined by IETF RFC 4291.
        constexpr bool is_unicast() const noexcept {
            return !this->is_multicast();
        }

        /// Returns true if the address is a unicast address with link-local scope.
        constexpr bool is_unicast_link_local() const noexcept {
            return (this->segments()[0] & 0xFFC0) == 0xFE80;
        }

        /// Returns true if this is an address reserved for documentation.
        constexpr bool is_documentation() const noexcept {
            auto segs = this->segments();
            return segs[0] == 0x2001 && segs[1] == 0xDB8;
        }

        /// Returns true if this is an address reserved for benchmarking (`2001:2::/48`).
        constexpr bool is_benchmarking() const noexcept {
            auto segs = this->segments();
            return segs[0] == 0x2001 && segs[1] == 0x2 && segs[2] == 0;
        }

        /// Returns true if the address is a globally routable unicast address.
        constexpr bool is_unicast_global() const noexcept {
            return this->is_unicast()
                && !this->is_loopback()
                && !this->is_unicast_link_local()
                && !this->is_unique_local()
                && !this->is_unspecified()
                && !this->is_documentation()
                && !this->is_benchmarking();
        }

        /// Returns true if the address appears to be globally reachable.
        constexpr bool is_global() const noexcept {
            auto segs = this->segments();
            return !(this->is_unspecified()
                // IPv4-mapped Address (`::ffff:0:0/96`)
                || (segs[0] == 0 && segs[1] == 0 && segs[2] == 0 && segs[3] == 0 && segs[4] == 0 && segs[5] == 0xFFFF)
                // IPv4-IPv6 Translat. (`64:ff9b:1::/48`)
                || (segs[0] == 0x64 && segs[1] == 0xFF9B && segs[2] == 1)
                // Discard-Only Address Block (`100::/64`)
                || (segs[0] == 0x100 && segs[1] == 0 && segs[2] == 0 && segs[3] == 0)
                // IETF Protocol Assignments (`2001::/23`)
                || ((segs[0] == 0x2001 && segs[1] < 0x200)
                    && !(
                        // Port Control Protocol Anycast (`2001:1::1`)
                        this->as_bits() == ::usync::impl::u128(0x2001'0001'0000'0000ULL, 0x0000'0000'0000'0001)
                        // Traversal Using Relays around NAT Anycast (`2001:1::2`)
                        || this->as_bits() == ::usync::impl::u128(0x2001'0001'0000'0000ULL, 0x0000'0000'0000'0002)
                        // AMT (`2001"4"112""/48`)
                        || (segs[0] == 0x2001 && segs[1] == 3)
                        // AS112-v6 (`2001:4:112::/48`)
                        || (segs[0] == 0x2001 && segs[1] == 4 && segs[2] == 0x112)
                        // ORCHIDv2 (`2001:20::/28`)
                        // Drone Remote ID Protocol Entity Tags (DETs) Prefix (`2001:30::/28`)
                        || (segs[0] == 0x2001 && (0x19 < segs[1] && segs[1] < 0x40))
                    ))
                // 6to4 (`2002::/16`) – it's not explicitly documented as globally reachable,
                // IANA says N/A.
                || (segs[0] == 0x2002)
                || this->is_documentation()
                || this->is_unique_local()
                || this->is_unicast_link_local());
        }

        /// Returns true if the address is an IPv4-mapped address (`::ffff:0:0/96`).
        constexpr bool is_ipv4_mapped() const noexcept {
            auto segs = this->segments();
            return segs[0] == 0 && segs[1] == 0 && segs[2] == 0 && segs[3] == 0 && segs[4] == 0 && segs[5] == 0xFFFF;
        }

        /// Converts this address to an IPv4 address if it's an IPv4-mapped address.
        constexpr std::optional<Ipv4Addr> to_ipv4_mapped() const noexcept {
            auto segs = this->segments();
            if (segs[0] == 0 && segs[1] == 0 && segs[2] == 0 && segs[3] == 0 && segs[4] == 0 && segs[5] == 0xFFFF) {
                return Ipv4Addr(m_octets[12], m_octets[13], m_octets[14], m_octets[15]);
            } else {
                return {};
            }
        }

        /// Converts this address to an IPv4 address, if possible.
        constexpr std::optional<Ipv4Addr> to_ipv4() const noexcept {
            auto segs = this->segments();
            if (segs[0] == 0 && segs[1] == 0 && segs[2] == 0 && segs[3] == 0 && segs[4] == 0 && (segs[5] == 0 || segs[5] == 0xFFFF)) {
                auto ab = ::usync::impl::to_big_endian_bytes<uint16_t>(segs[6]);
                auto cd = ::usync::impl::to_big_endian_bytes<uint16_t>(segs[7]);

                return Ipv4Addr(ab[0], ab[1], cd[0], cd[1]);
            } else {
                return {};
            }
        }

        /// Converts this address into an `in6_addr` instance.
        constexpr in6_addr to_in6_addr() const noexcept {
            return std::bit_cast<in6_addr>(m_octets);
        }
    };

    constexpr Ipv6Addr Ipv4Addr::to_ipv6_compatible() const noexcept {
        return Ipv6Addr{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, m_octets[0], m_octets[1], m_octets[2], m_octets[3]}};
    }

    constexpr Ipv6Addr Ipv4Addr::to_ipv6_mapped() const noexcept {
        return Ipv6Addr{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xFF, 0xFF, m_octets[0], m_octets[1], m_octets[2], m_octets[3]}};
    }

    namespace ipv4 {

        /// An IPv4 address with the address pointing to localhost: `127.0.0.1`.
        constexpr inline Ipv4Addr Localhost{127, 0, 0, 1};

        /// An IPv4 address representing an unspecified address: `0.0.0.0`.
        constexpr inline Ipv4Addr Unspecified{0, 0, 0, 0};

        /// An IPv4 address representing the broadcast address: `255.255.255.255`.
        constexpr inline Ipv4Addr Broadcast{255, 255, 255, 255};

    }

    namespace ipv6 {

        /// An IPv6 address representing localhost: `::1`.
        constexpr inline Ipv6Addr Localhost{0, 0, 0, 0, 0, 0, 0, 1};

        /// An IPv6 address representing the unspecified address: `::`.
        constexpr inline Ipv6Addr Unspecified{0, 0, 0, 0, 0, 0, 0, 0};

    }

}
