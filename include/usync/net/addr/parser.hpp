#pragma once

#include <array>
#include <cstdint>
#include <limits>
#include <span>
#include <string_view>
#include <utility>

namespace usync::net::impl {

    constexpr char to_lower(char c) noexcept {
        return ('A' <= c && c <= 'Z') ? static_cast<char>(c - 'A' + 'a') : c;
    }

    constexpr bool is_digit(char c) noexcept {
        return '0' <= c && c <= '9';
    }

    constexpr bool is_hex_digit(char c) noexcept {
        c = to_lower(c);
        return is_digit(c) || ('a' <= c && c <= 'f');
    }

    template <int Radix> requires (Radix == 8 || Radix == 10 || Radix == 16)
    constexpr bool validate_digit(char c) noexcept {
        if constexpr (Radix == 8) {
            // Octal digits.
            return '0' <= c && c <= '7';
        } else if constexpr (Radix == 10) {
            // Decimal digits.
            return is_digit(c);
        } else {
            // Hexadecimal digits.
            static_assert(Radix == 16);
            return is_hex_digit(c);
        }
    }

    template <int Radix> requires (Radix == 8 || Radix == 10 || Radix == 16)
    constexpr int convert_digit(char c) noexcept {
        // Make sure we accept a valid character.
        if (!validate_digit<Radix>(c)) {
            return -1;
        }

        // Do the char -> int conversion based on Radix.
        if constexpr (Radix == 8 || Radix == 10) {
            // Decimal and octal digits.
            return c - '0';
        } else {
            // Hexadecimal digits.
            static_assert(Radix == 16);
            if (is_digit(c)) {
                return convert_digit<10>(c);
            } else {
                c = to_lower(c);
                return (c - 'a' + 10);
            }
        }
    }

    struct IpAddressParser {
    private:
        std::string_view m_input;

    public:
        constexpr explicit IpAddressParser(std::string_view in) noexcept
            : m_input(in) {}

    private:
        constexpr int peek() const noexcept {
            if (m_input.empty()) {
                return -1;
            } else {
                return m_input[0];
            }
        }

        constexpr int next() noexcept {
            if (m_input.empty()) {
                return -1;
            } else {
                char c  = m_input[0];
                m_input = m_input.substr(1);
                return c;
            }
        }

        template <int Radix>
        constexpr int next_digit() noexcept {
            int c = this->next();
            if (c == -1) {
                return -1;
            }

            return convert_digit<Radix>(c);
        }

    private:
        // Runs `fn` with the parser and restores original state on error.
        template <class Fn>
        constexpr int atomic_parse(Fn fn) noexcept {
            std::string_view backup{m_input};

            int res = fn();
            if (res == -1) {
                m_input = backup;
            }

            return res;
        }

        template <int Radix, typename T, unsigned MaxDigits = 0>
        constexpr int read_number(T *out, bool allow_zero_prefix = false) noexcept {
            return this->atomic_parse([&]() {
                int buf;
                uint64_t res = 0;
                int digits   = 0;
                bool zero    = this->peek() == '0';

                // Consume digits while available.
                while ((buf = this->atomic_parse([&]() { return this->next_digit<Radix>(); })) != -1) {
                    // Make sure we didn't exceed the digit limit yet.
                    ++digits;
                    if (MaxDigits > 0 && digits > MaxDigits) {
                        return -1;
                    }

                    // Accumulate the digit into the result.
                    res *= Radix;
                    res += buf;

                    // Make sure we didn't exceed the value limit yet.
                    if (res > std::numeric_limits<T>::max()) {
                        return -1;
                    }
                }

                // Parsing nothing is treated like an error.
                if (digits == 0) {
                    return -1;
                }

                // If we got a leading zero in an invalid context, bail out.
                if (!allow_zero_prefix && zero && digits > 1) {
                    return -1;
                }

                *out = res;
                return 0;
            });
        }

    public:
        constexpr bool match(char c) noexcept {
            int res = this->atomic_parse([&]() { return std::cmp_equal(this->next(), static_cast<uint8_t>(c)) ? 0 : -1; });
            return res != -1;
        }

    private:
        // Parses `sep` on request, then runs the `fn` parser.
        template <class Fn>
        constexpr int parse_separated(char sep, bool should_parse, Fn fn) noexcept {
            return this->atomic_parse([&]() {
                if (should_parse && !this->match(sep)) {
                    return -1;
                }
                return fn();
            });
        }

    public:
        constexpr int parse_ipv4_addr(std::array<uint8_t, 4> &out) noexcept {
            return this->atomic_parse([&]() {
                uint8_t a = 0, b = 0, c = 0, d = 0;

                int res = this->read_number<10, uint8_t, 3>(&a);
                res += this->parse_separated('.', true, [&]() { return this->read_number<10, uint8_t, 3>(&b); });
                res += this->parse_separated('.', true, [&]() { return this->read_number<10, uint8_t, 3>(&c); });
                res += this->parse_separated('.', true, [&]() { return this->read_number<10, uint8_t, 3>(&d); });

                // Make sure no parsing errors were encountered while reading.
                if (res < 0) {
                    return -1;
                }

                out[0] = a;
                out[1] = b;
                out[2] = c;
                out[3] = d;
                return 0;
            });
        }

    private:
        constexpr void read_groups(std::span<uint16_t> out, size_t *len, bool *is_ipv4) noexcept {
            auto limit = out.size();
            for (size_t i = 0; i < limit; ++i) {
                // Try to read a trailing embedded IPv4 address.
                // There must be at least two groups left.
                if (i < (limit - 1)) {
                    std::array<uint8_t, 4> ipv4{};
                    if (this->parse_separated(':', i > 0, [&]() { return this->parse_ipv4_addr(ipv4); }) != -1) {
                        out[i + 0] = (static_cast<uint16_t>(ipv4[0]) << 8) | static_cast<uint16_t>(ipv4[1]);
                        out[i + 1] = (static_cast<uint16_t>(ipv4[2]) << 8) | static_cast<uint16_t>(ipv4[3]);

                        *len     = i + 2;
                        *is_ipv4 = true;
                        return;
                    }
                }

                uint16_t value;
                if (this->parse_separated(':', i > 0, [&]() { return this->read_number<16, uint16_t, 4>(&value, true); }) != -1) {
                    out[i] = value;
                } else {
                    *len     = i;
                    *is_ipv4 = false;
                    return;
                }
            }

            *len     = limit;
            *is_ipv4 = false;
        }

    public:
        constexpr int parse_ipv6_addr(std::array<uint16_t, 8> &out) noexcept {
            return this->atomic_parse([&]() {
                // Read the front part of the address; either the whole thing,
                // or up to the first `::`.
                size_t head_size;
                bool head_ipv4;
                this->read_groups(out, &head_size, &head_ipv4);

                if (head_size == 8) {
                    return 0;
                }

                // IPv4 part is not allowed before `::`.
                if (head_ipv4) {
                    return -1;
                }

                // Read `::` if previous code parsed less than 8 groups.
                // `::` indicates one or more groups of 16 bits of zeroes.
                if (!(this->match(':') && this->match(':'))) {
                    return -1;
                }

                // Read the back part of the address. The `::` must contain
                // at least one set of zeroes, so our max length is 7.
                size_t tail_size;
                bool tail_ipv4;
                std::array<uint16_t, 7> tail{};
                this->read_groups(std::span(tail.data(), 8 - (head_size + 1)), &tail_size, &tail_ipv4);

                // Concat the head and tail of the IP address.
                for (size_t i = 8 - tail_size; i < 8; ++i) {
                    out[i] = tail[i - (8 - tail_size)];
                }

                return 0;
            });
        }

        constexpr int parse_port(uint16_t *out) noexcept {
            return this->parse_separated(':', true, [&]() {
                uint16_t port = 0;
                if (this->read_number<10>(&port, true) != -1) {
                    *out = port;
                    return 0;
                }

                return -1;
            });
        }

        constexpr int parse_scope_id(uint32_t *out) noexcept {
            return this->parse_separated('%', true, [&]() {
                uint32_t scope_id = 0;
                if (this->read_number<10>(&scope_id, true) != -1) {
                    *out = scope_id;
                    return 0;
                }

                return -1;
            });
        }

    public:
        template <class Fn>
        constexpr int parse_full(Fn fn) noexcept {
            int res = fn();
            if (res == -1 || !m_input.empty()) {
                return -1;
            }

            return res;
        }
    };

}
