#pragma once

#include <chrono>
#include <optional>
#include <utility>

#include <sys/socket.h>

#include "usync/task.hpp"

namespace usync::net::impl {

    enum class Shutdown {
        Write,
        Read,
        Both,
    };

    class Socket {
    private:
        int m_fd;

    public:
        inline explicit Socket(int fd) noexcept : m_fd(fd) {}

        ~Socket() noexcept;

        // Cannot copy open file descriptors.
        Socket(const Socket &) = delete;
        Socket &operator=(const Socket &) = delete;

        inline Socket(Socket &&rhs) noexcept : m_fd(std::exchange(rhs.m_fd, -1)) {}

        Socket &operator=(Socket &&rhs) noexcept;

        [[nodiscard]] inline int fd() const noexcept {
            return m_fd;
        }

    public:
        static Socket make_raw(int domain, int type);

    public:
        void shutdown(Shutdown how) const;

        [[nodiscard]] std::optional<std::chrono::nanoseconds> get_timeout(int kind) const;
        [[nodiscard]] std::optional<std::chrono::seconds> get_linger() const;
        [[nodiscard]] bool get_reuseaddr() const;
        [[nodiscard]] bool get_nodelay() const;
        [[nodiscard]] bool get_quickack() const;
        [[nodiscard]] bool get_passcred() const;
        [[nodiscard]] int get_ttl() const;

        void set_timeout(int kind, std::optional<std::chrono::nanoseconds> timeout) const;
        void set_linger(std::optional<std::chrono::seconds> linger) const;
        void set_reuseaddr(bool flag) const;
        void set_nodelay(bool flag) const;
        void set_quickack(bool flag) const;
        void set_passcred(bool flag) const;
        void set_ttl(int value) const;
        void set_nonblocking(bool flag) const;
        void set_mark(int mark) const;

        void check_error() const;

    public:
        Task<> connect(const sockaddr *addr, socklen_t len) const;

        Task<Socket> accept(sockaddr *storage, socklen_t *len) const;

        Task<int> recv(void *buf, size_t len, int flags = 0) const;

        Task<int> send(const void *buf, size_t len, int flags = 0) const;
    };

}
