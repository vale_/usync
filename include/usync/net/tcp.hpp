#pragma once

#include "usync/net/impl/socket.hpp"
#include "usync/net/addr.hpp"

namespace usync::net {

    namespace impl {

        class TcpBase {
        protected:
            impl::Socket m_socket;

        protected:
            explicit TcpBase(impl::Socket socket) noexcept
                : m_socket(std::move(socket)) {}

        public:
            /// Returns the \ref `SocketAddr` of the local half of this connection.
            [[nodiscard]] SocketAddr local_addr() const;

            /// Gets the value for the `IP_TTL` option on this socket.
            [[nodiscard]] inline int ttl() const {
                return m_socket.get_ttl();
            }

            /// Sets the value for the `IP_TTL` option on this socket.
            ///
            /// \param value The time-to-live field that will be sent in every packet.
            inline void set_ttl(int value) const {
                m_socket.set_ttl(value);
            }

            /// Gets the value of the `SO_ERROR` option on this socket
            /// as a C++ runtime exception.
            inline void check_error() const {
                m_socket.check_error();
            }
        };

    }

    using impl::Shutdown;

    /// A TCP socket server, listening for connections.
    ///
    /// After binding a listener, it awaits incoming TCP connections from
    /// client sockets. They can be accepted by calling the accept method
    /// in a loop.
    class TcpListener;

    /// A TCP stream between a local and a remote socket.
    ///
    /// A stream is created by either connecting to a host or accepting
    /// a connection on a listener. Afterwards, data can be read and
    /// written over the connection.
    class TcpStream : impl::TcpBase {
        friend class TcpListener;

    private:
        explicit TcpStream(impl::Socket socket) noexcept
            : impl::TcpBase(std::move(socket)) {}

    public:
        /// Gets the read timeout on the underlying socket.
        [[nodiscard]] inline std::optional<std::chrono::nanoseconds> read_timeout() const {
            return m_socket.get_timeout(SO_RCVTIMEO);
        }

        /// Gets the write timeout on the underlying socket.
        [[nodiscard]] inline std::optional<std::chrono::nanoseconds> write_timeout() const {
            return m_socket.get_timeout(SO_SNDTIMEO);
        }

        /// Sets the read timeout on the underlying socket.
        inline void set_read_timeout(std::optional<std::chrono::nanoseconds> timeout) const {
            m_socket.set_timeout(SO_RCVTIMEO, timeout);
        }

        /// Sets the write timeout on the underlying socket.
        inline void set_write_timeout(std::optional<std::chrono::nanoseconds> timeout) const {
            m_socket.set_timeout(SO_SNDTIMEO, timeout);
        }

        /// Gets the peer address this socket is connected to.
        [[nodiscard]] SocketAddr peer_addr() const;

        /// Shuts the socket halves down with the given strategy.
        inline void shutdown(Shutdown how) const {
            m_socket.shutdown(how);
        }

        /// Gets the value of the `SO_LINGER` option on this socket.
        [[nodiscard]] inline std::optional<std::chrono::seconds> linger() const {
            return m_socket.get_linger();
        }

        /// Sets the value of the `SO_LINGER` option on this socket.
        ///
        /// The duration controls how long the socket should stay open as the system
        /// reattempts to send pending data when the socket is closed.
        inline void set_linger(std::optional<std::chrono::seconds> linger) const {
            m_socket.set_linger(linger);
        }

        /// Gets the value of the `TCP_NODELAY` option on this socket.
        [[nodiscard]] inline bool nodelay() const {
            return m_socket.get_nodelay();
        }

        /// Sets the value of the `TCP_NODELAY` option on this socket.
        ///
        /// \param flag Whether the Nagle algorithm should be disabled.
        inline void set_nodelay(bool flag) const {
            m_socket.set_nodelay(flag);
        }

    public:
        /// Connects to the given socket address in order to obtain a TCP stream.
        static Task<TcpStream> connect(const SocketAddr &addr);

        /// Attempts to read data from the socket.
        ///
        /// \param buf A reserved output buffer for the data.
        /// \param len The size of the buffer in bytes.
        /// \return How many bytes were read into the buffer.
        Task<size_t> read(void *buf, size_t len) const;

        /// Attempts to write data over the socket.
        ///
        /// \param buf The data buffer to send.
        /// \param len The amount of bytes in the buffer.
        /// \return How many bytes were actually sent.
        Task<size_t> write(const void *buf, size_t len) const;
    };

    class TcpListener : impl::TcpBase {
    private:
        explicit TcpListener(impl::Socket socket) noexcept
            : impl::TcpBase(std::move(socket)) {}

    public:
        /// Creates a new TCP listener bound to a specific address.
        ///
        /// The returned listener is ready to accept client connections
        /// and yields them as \ref `TcpStream`s.
        ///
        /// Using a port of 0 will make the OS assign a port. The
        /// `local_addr` method can be used to query the allocation.
        static TcpListener bind(const SocketAddr &addr);

        /// Accepts a new incoming connection on this listener.
        ///
        /// This function will wait until a new TCP connection to
        /// the socket is established. The \ref TcpStream and the
        /// remote address for the new connection will be returned.
        Task<std::pair<TcpStream, SocketAddr>> accept() const;
    };

}
