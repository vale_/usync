#pragma once

#include <coroutine>
#include <memory>
#include <stdexcept>

#include <liburing.h>

#include "impl/chrono_util.hpp"
#include "impl/uring_error.hpp"

namespace usync {

    struct RuntimeParams;

    /// Reactor implementation which handles asynchronous I/O in the runtime.
    ///
    /// Reactors manage an instance of `io_uring` and provide mechanisms to
    /// populate the entries of the ring's Submission Queue. Each entry is
    /// marked with a continuation so we can resume the coroutine blocked
    /// on it once we receive the result in the Completion Queue.
    ///
    /// Reactors are exclusively meant for single-threaded usage. Do not
    /// share a Reactor with multiple threads and only submit operations
    /// from the thread the reactor was created on.
    class Reactor {
    private:
        io_uring m_ring;

    public:
        explicit Reactor(const RuntimeParams &params);

        ~Reactor() noexcept;

        // Forbid copying the Reactor since I/O rings are unique.
        Reactor(const Reactor &) = delete;
        Reactor &operator=(const Reactor &) = delete;

    private:
        struct Continuation {
            std::coroutine_handle<> m_handle;
            int m_io_result;
        };

        class SqeFuture {
        private:
            io_uring_sqe *m_sqe;
            Continuation m_continuation;

        public:
            inline explicit SqeFuture(io_uring_sqe *sqe) noexcept
                : m_sqe(sqe) {}

        public:
            constexpr bool await_ready() const noexcept {
                // The SQE entry is yet to be submitted to the kernel,
                // so it's not possible to avoid the suspension here.
                return false;
            }

            inline void await_suspend(std::coroutine_handle<> h) noexcept {
                // When we're suspending, mark the SQE entry with our
                // continuation. When the entry then completes, we can
                // resume the awaiter through the stored handle.
                m_continuation.m_handle = h;
                io_uring_sqe_set_data(m_sqe, &m_continuation);
            }

            constexpr int await_resume() const noexcept {
                // We were resumed, so now we have the operation's result.
                // I/O abstractions should turn it into an exception.
                return m_continuation.m_io_result;
            }
        };

    private:
        int wait_timeout(__kernel_timespec *ts) noexcept;

    public:
        /// Submits pending I/O operations to the ring without waiting.
        inline void submit() noexcept {
            io_uring_submit(&m_ring);
        }

        /// Submits outstanding I/O operations and blocks for completions.
        inline void wait() noexcept {
            int res = io_uring_submit_and_wait(&m_ring, 1);
            if (res < 0) {
                impl::throw_io_uring_error(res, "Reactor::wait");
            }
        }

        /// Submits outstanding I/O operations and blocks for completions
        /// until one becomes available or the call times out.
        template <typename Clock, typename Dur>
        inline int wait_until(const std::chrono::time_point<Clock, Dur> &timeout) noexcept {
            auto ts = impl::duration_to_timespec(timeout - Clock::now());
            return this->wait_timeout(&ts);
        }

        /// Retrieves completed events and dispatched them to blocked tasks.
        void process_events();

    private:
        io_uring_sqe *get_sqe_entry();

        template <typename Fn, typename... Args>
        inline auto prepare_op(Fn fn, Args &&...args) {
            auto *sqe = this->get_sqe_entry();
            fn(sqe, std::forward<Args>(args)...);

            return SqeFuture(sqe);
        }

    public:
        /// Prepares a nop entry in the Submission Queue.
        inline auto nop() {
            return this->prepare_op(io_uring_prep_nop);
        }

        /// Prepares an accept entry in the Submission Queue.
        inline auto accept(int fd, sockaddr *addr, socklen_t *len, int flags) {
            return this->prepare_op(io_uring_prep_accept, fd, addr, len, flags);
        }

        /// Prepares a connect entry in the Submission Queue.
        inline auto connect(int fd, const sockaddr *addr, socklen_t len) {
            return this->prepare_op(io_uring_prep_connect, fd, addr, len);
        }

        /// Prepares a recv entry in the Submission Queue.
        inline auto recv(int fd, void *buf, size_t len, int flags) {
            return this->prepare_op(io_uring_prep_recv, fd, buf, len, flags);
        }

        /// Prepares a send entry in the Submission Queue.
        inline auto send(int fd, const void *buf, size_t len, int flags) {
            return this->prepare_op(io_uring_prep_send, fd, buf, len, flags);
        }
    };

}
