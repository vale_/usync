#pragma once

#include <deque>

#include "reactor.hpp"
#include "runtime_params.hpp"
#include "task.hpp"
#include "time/timer_queue.hpp"

namespace usync {

    class Runtime {
    private:
        Reactor m_reactor;
        TimerQueue m_timer_queue;
        std::deque<std::coroutine_handle<>> m_run_queue;

    private:
        // By design, our runtime is single-threaded. We use this to
        // store different active runtimes on a per-thread basis.
        inline static thread_local Runtime *g_runtime;

    public:
        /// Constructs a new runtime on this thread with the given config.
        explicit Runtime(const RuntimeParams &params = {});

        // Moving the runtime around is not allowed. It should stay
        // where it was initially created for `g_runtime`.
        Runtime(Runtime &&) = delete;
        Runtime &operator=(Runtime &&) = delete;

        /// Returns a reference to the associated reactor.
        inline Reactor &reactor() noexcept {
            return m_reactor;
        }

    public:
        /// Gets the global runtime instance, if the caller is currently
        /// executing from inside the runtime context.
        static Runtime &instance();

        /// Submits a coroutine handle to be resumed at the next tick.
        inline void spawn_task(std::coroutine_handle<> h) {
            m_run_queue.push_back(h);
        }

        /// Gets a yet to be submitted timer entry for modification.
        inline TimerEntry timer_entry(const std::chrono::steady_clock::time_point  &deadline) noexcept {
            return m_timer_queue.entry(deadline);
        }

        /// Submits a timer entry to be managed by the runtime.
        ///
        /// On completion, it resumes the continuation coroutine.
        inline void submit_timer(TimerEntry &entry) noexcept {
            m_timer_queue.push(entry);
        }

        /// Blocks the current thread on the task and drives it to completion.
        ///
        /// For the duration of this call, the runtime context will be set up
        /// and the runtime will be ticked. The supplied coroutine may spawn
        /// more asynchronous tasks.
        ///
        /// \tparam T The return type of the task.
        /// \param task The task of the coroutine to block on.
        /// \return The return value of the coroutine.
        template <typename T>
        T block_on(Task<T> task) {
            g_runtime = this;

            // When our tasks enter final suspension, they pass execution
            // on to their awaiter. However, the first coroutine in the
            // hierarchy doesn't have one, so we give it a fake one here.
            task.handle().promise().set_awaiter(std::noop_coroutine());

            // Poll the coroutine to make initial progress, then tick the
            // runtime for the proper run loop.
            task.resume();
            while (!task.done()) {
                this->tick(task.handle());
            }

            g_runtime = nullptr;
            return task.get_return_value();
        }

    private:
        void process_timers();

        void tick(std::coroutine_handle<> root);
    };

}
