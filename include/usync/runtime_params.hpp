#pragma once

namespace usync {

    /// Configuration for a usync runtime instance.
    struct RuntimeParams final {
        /// The number of entries in the ring's Submission Queue.
        unsigned int SqEntries = 256;
        /// The number of entries in the ring's Completion Queue.
        unsigned int CqEntries = 256;
    };

}
