#pragma once

#include <coroutine>
#include <vector>

namespace usync {

    /// Event synchronization primitive for tasks.
    class Event {
    private:
        bool m_flag;
        std::vector<std::coroutine_handle<>> m_awaiters;

    public:
        /// Constructs a new, cleared event.
        constexpr Event() : m_flag(false), m_awaiters() {}

        /// Sets the event flag and dispatches blocked awaiters.
        inline void set() {
            m_flag = true;
            for (auto &handle : m_awaiters) {
                handle.resume();
            }
            m_awaiters.clear();
        }

        /// Clears the event flag.
        inline void clear() noexcept {
            m_flag = false;
        }

        /// Checks if the event is currently set.
        inline bool is_set() const noexcept {
            return m_flag;
        }

        /// Returns an awaitable to wait for an event to be set.
        inline auto wait() noexcept {
            struct Impl {
                Event &event;

                inline bool await_ready() const noexcept {
                    return event.is_set();
                }

                void await_suspend(std::coroutine_handle<> h) const noexcept {
                    event.m_awaiters.push_back(h);
                }

                inline void await_resume() const noexcept {}
            };

            return Impl{*this};
        }
    };

}
