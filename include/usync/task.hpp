#pragma once

#include <coroutine>
#include <stdexcept>

#include "impl/coro_owner.hpp"

namespace usync {

    /// The asynchronous unit of work in the usync runtime.
    ///
    /// Tasks are the logical owners of a coroutine's entire state, therefore
    /// a coroutine's lifetime is tied to that of its task. They provide the
    /// facilities to interact and introspect that state.
    ///
    /// The event loop must check for return values and uncaught exceptions
    /// on a coroutine's Task object.
    ///
    /// \tparam T The return type this coroutine will resolve to.
    template <typename T = void>
    class Task;

    namespace impl {

        /// Shared behavior for all promise types with CRTP.
        template <typename Self, typename T>
        struct PromiseBase {
            using handle_type = std::coroutine_handle<Self>;

        private:
            std::coroutine_handle<> m_awaiter;

            struct ResumeFuture {
                constexpr bool await_ready() const noexcept {
                    // This coroutine must always suspend to yield control
                    // back to the awaiter and must never resume again.
                    return false;
                }

                template <typename P>
                constexpr std::coroutine_handle<> await_suspend(std::coroutine_handle<P> h) const noexcept {
                    // Resume the previous awaiter registered for the `Task`.
                    return h.promise().m_awaiter;
                }

                constexpr void await_resume() const noexcept {
                    // If we ever land here, it is a bug. After yielding control,
                    // the coroutine cannot resume again.
                }
            };

        public:
            /// Stores a handle to resume when this coroutine finishes.
            constexpr void set_awaiter(std::coroutine_handle<> h) noexcept {
                m_awaiter = h;
            }

        public:
            inline Task<T> get_return_object() {
                auto handle = handle_type::from_promise(*static_cast<Self*>(this));
                return Task<T>(handle);
            }

            inline std::suspend_always initial_suspend() const noexcept {
                // Our coroutines are lazy. They do nothing before initially resumed.
                return {};
            }

            inline auto final_suspend() const noexcept {
                // Transfer execution back to the coroutine that awaited us.
                return ResumeFuture{};
            }

            inline void unhandled_exception() noexcept {
                // TODO: How do we want to handle exceptions?
            }
        };

        /// Promise type for coroutines returning values.
        template <typename T>
        struct Promise : PromiseBase<Promise<T>, T> {
        private:
            struct Empty {};

            union {
                Empty m_empty;
                T m_value;
            };

        public:
            constexpr Promise() noexcept : m_empty() {}

            constexpr ~Promise() {
                // Empty is trivially destructible, `T` is moved out before
                // the object is destroyed. We have nothing to do here.
            }

            constexpr void return_value(T &&value) noexcept {
                m_value = std::move(value);
            }

            constexpr T &&get_value() noexcept {
                return std::move(m_value);
            }
        };

        /// Promise type for coroutines returning `void`.
        template <>
        struct Promise<void> : PromiseBase<Promise<void>, void> {
            constexpr void return_void() const noexcept {}
        };

    }

    template <typename T>
    class [[nodiscard]] Task final : impl::CoroutineOwner {
    public:
        using promise_type = impl::Promise<T>;
        using handle_type  = promise_type::handle_type;

    private:
        friend struct impl::PromiseBase<promise_type, T>;

        constexpr explicit Task(handle_type h) noexcept
            : impl::CoroutineOwner(h) {}

    public:
        /// Gets the coroutine handle for this task.
        constexpr handle_type handle() const noexcept {
            return handle_type::from_address(m_handle.address());
        }

        /// Indicates if the coroutine is finished yet.
        inline bool done() const noexcept {
            return m_handle.done();
        }

        /// Resumes the coroutine execution from suspended state.
        void resume() {
            if (m_handle.done()) [[unlikely]] {
                throw std::runtime_error("resumed coroutine that was done already");
            }
            m_handle.resume();
        }

        inline T get_return_value() noexcept {
            if constexpr (!std::is_same_v<T, void>) {
                return this->handle().promise().get_value();
            }
        }

    public:
        // `co_await` support for Tasks.

        inline bool await_ready() const noexcept {
            // Avoid suspending when this task has no more work left.
            return this->done();
        }

        inline auto await_suspend(std::coroutine_handle<> h) noexcept {
            // Record the awaiter and start executing ourselves.
            this->handle().promise().set_awaiter(h);
            return m_handle;
        }

        inline T await_resume() noexcept {
            // When we're done, deliver the recorded return value.
            return this->get_return_value();
        }
    };

}
