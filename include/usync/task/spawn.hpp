#pragma once

#include "usync/task.hpp"

namespace usync {

    namespace impl {

        void spawn(std::coroutine_handle<> h);

    }

    /// An owned permit to await the completion of a background task.
    ///
    /// \tparam T The result type delivered by the background task.
    template <typename T = void>
    class [[nodiscard]] JoinHandle final {
        template <typename U>
        friend auto spawn(Task<U> task);

    private:
        Task<T> m_task;

    private:
        inline explicit JoinHandle(Task<T> &&t) : m_task(std::forward<Task<T>>(t)) {}

    public:
        inline auto operator co_await() {
            struct Impl {
                Task<T> &m_task;

                inline bool await_ready() const noexcept {
                    // Suspend only if the spawned task is not done.
                    return m_task.done();
                }

                inline void await_suspend(std::coroutine_handle<> h) const noexcept {
                    // The awaiter is blocked on the completion of the task
                    // in the background, so we let the task resume it.
                    m_task.handle().promise().set_awaiter(h);
                }

                inline T await_resume() const noexcept {
                    // We were woken, so we can deliver the return value.
                    return m_task.get_return_value();
                }
            };

            return Impl{m_task};
        }
    };

    /// Spawns a task onto the runtime in the background.
    template <typename T>
    inline auto spawn(Task<T> task) {
        // Need an awaiter for now, anything will do.
        // Will be replaced with a real one when handle gets awaited.
        task.handle().promise().set_awaiter(std::noop_coroutine());

        impl::spawn(task.handle());
        return JoinHandle<T>(std::move(task));
    }

}
