#pragma once

#include "usync/task.hpp"

namespace usync {

    /// Yields execution back to the usync runtime when awaited.
    ///
    /// This will allow other tasks to run before the current one
    /// is able to make progress again.
    struct yield_now final {
        inline bool await_ready() const noexcept {
            // Must suspend in order to yield.
            return false;
        }

        void await_suspend(std::coroutine_handle<> h) const noexcept;

        inline void await_resume() const noexcept {}
    };

}
