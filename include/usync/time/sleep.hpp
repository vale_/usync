#pragma once

#include <chrono>

#include "usync/task.hpp"
#include "usync/time/timer_queue.hpp"

namespace usync {

    namespace impl {

        class Sleep {
        private:
            TimerEntry m_entry;

        public:
            explicit Sleep(const std::chrono::steady_clock::time_point &deadline) noexcept;

        public:
            inline bool await_ready() const noexcept {
                // Avoid suspending if the entry's deadline is expired.
                // TODO: Is the overhead of the call worth even checking it?
                return m_entry.deadline() < std::chrono::steady_clock::now();
            }

            void await_suspend(std::coroutine_handle<> h) noexcept;

            inline void await_resume() const noexcept {}
        };

    }

    /// Sleeps until a given point in time is reached.
    ///
    /// This suspends execution of the current coroutine and allows other
    /// tasks to make progress until the coroutine wakes up again after
    /// the specified point in time.
    inline auto sleep_until(const std::chrono::steady_clock::time_point &tp) noexcept {
        return impl::Sleep(tp);
    }

    /// Sleeps for a given duration.
    ///
    /// Given a duration, this suspends execution of the current coroutine
    /// for at least that long and allows other tasks to make progress in
    /// the meantime.
    template <typename Rep, typename Period>
    inline auto sleep(const std::chrono::duration<Rep, Period> &dur) noexcept {
        return sleep_until(std::chrono::steady_clock::now() + dur);
    }

}
