#pragma once

#include <chrono>
#include <coroutine>

#include "usync/impl/binary_heap.hpp"

namespace usync {

    /// A priority queue managing timer expirations.
    ///
    /// Since all its entries are stored in the coroutine frames which are
    /// blocked on timer expiration, it fully avoids dynamic allocations
    /// by intrusively linking the entries together.
    ///
    /// The queue always yields the timer closest to expiration and is able
    /// to resume the coroutine handles of expired timers.
    class TimerQueue;

    /// An intrusive link representing a timer in the queue.
    ///
    /// Users should not use this class directly and instead the facilities
    /// which build around it.
    class TimerEntry : public impl::IntrusiveHeapBaseNode<TimerEntry> {
        friend class TimerQueue;

    private:
        size_t m_id;
        std::coroutine_handle<> m_continuation;
        std::chrono::steady_clock::time_point m_deadline;

    private:
        constexpr TimerEntry(size_t id, std::chrono::steady_clock::time_point tp) noexcept
            : m_id(id), m_deadline(tp) {}

    public:
        // Determines priorities in the queue with `std::less<TimerEntry>`.
        constexpr bool operator<(const TimerEntry &rhs) const noexcept {
            if (m_deadline < rhs.m_deadline) {
                return true;
            } else if (rhs.m_deadline < m_deadline) {
                return false;
            }

            // When two timers fire at the same time, favor the one that came first.
            return m_id < rhs.m_id;
        }

        /// Configures a continuation to resume when the timer fires.
        constexpr void set_continuation(std::coroutine_handle<> h) noexcept {
            m_continuation = h;
        }

        /// Resumes the coroutine blocked on this timer.
        inline void resume() const {
            m_continuation.resume();
        }

        /// Gets the deadline where this timer expires.
        constexpr const std::chrono::steady_clock::time_point &deadline() const noexcept {
            return m_deadline;
        }
    };

    class TimerQueue : public impl::BinaryHeap<TimerEntry> {
    private:
        size_t m_id_generator;

    public:
        constexpr TimerQueue() noexcept : BinaryHeap(), m_id_generator(0) {}

        // Disallow copying the queue as copies modify the same structure.
        TimerQueue(const TimerQueue &) = delete;
        TimerQueue &operator=(const TimerQueue &) = delete;

    public:
        /// Creates a new timer which can be submitted to the queue.
        constexpr auto entry(const std::chrono::steady_clock::time_point &deadline) noexcept {
            return TimerEntry(m_id_generator++, deadline);
        }

        /// Resumes pending coroutines blocked on elapsed timers.
        ///
        /// All timers that expired before `now` will fire. The caller
        /// must make sure to call this on non-empty timer queues only.
        constexpr void tick(const std::chrono::steady_clock::time_point &now) {
            do {
                auto &timer = this->peek();
                if (timer.m_deadline < now) {
                    // The timer has expired in the past. Remove it from
                    // the queue and resume the pending coroutine.
                    this->remove(timer);
                    timer.resume();
                } else {
                    // When no more expired timers are left, we're done.
                    break;
                }
            } while (!this->is_empty());
        }
    };

}
