#include <cstdio>

#include "usync/runtime.hpp"
#include "usync/task/spawn.hpp"
#include "usync/time/sleep.hpp"
#include "usync/net/tcp.hpp"

using namespace std::chrono_literals;

constinit usync::net::SocketAddr g_localhost{usync::net::ipv4::Localhost, 1337};

usync::Task<int> async_main();

int main() {
    usync::Runtime rt{};
    return rt.block_on(async_main());
}

usync::Task<> client_handler(int id, usync::net::TcpStream socket);

usync::Task<int> async_main() {
    //std::printf("a\n");
    //co_await usync::sleep(5s);
    //std::printf("b\n");

    try {
        auto server = usync::net::TcpListener::bind(g_localhost);

        int id_counter = 1;
        std::vector<usync::JoinHandle<>> pending;
        while (true) {
            auto [client, addr] = co_await server.accept();
            pending.push_back(usync::spawn(client_handler(id_counter++, std::move(client))));
        }

        for (auto &handle : pending) {
            co_await handle;
        }
    } catch(std::runtime_error &what) {
        std::printf("%s\n", what.what());
        co_return 3;
    }

    co_return 0;
}

usync::Task<> client_handler(int id, usync::net::TcpStream socket) {
    std::printf("client %d connected\n", id);
    char msgbuf[128];
    try {
        while (true) {
            size_t len = co_await socket.read(&msgbuf, 127);
            msgbuf[len] = '\0';
            std::printf("client %d sent: %s\n", id, msgbuf);
            co_await socket.write(msgbuf, len);
        }
    } catch (std::runtime_error &what) {
        std::printf("client %d errored: %s\n", id, what.what());
    }
}
