#include "usync/impl/uring_error.hpp"

#include <system_error>

namespace usync::impl {

    [[gnu::cold]] void throw_io_uring_error(int res, const char *func) {
        throw std::system_error(-res, std::generic_category(), func);
    }

}
