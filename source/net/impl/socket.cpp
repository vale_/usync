#include "usync/net/impl/socket.hpp"

#include <system_error>

#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

#include "usync/runtime.hpp"

namespace usync::net::impl {

    namespace {

        template <typename T>
        void setsockopt(int fd, int level, int optname, T value) {
            int res = ::setsockopt(fd, level, optname, &value, sizeof(T));
            if (res == -1) {
                throw std::system_error(errno, std::generic_category(), "setsockopt");
            }
        }

        template <typename T>
        T getsockopt(int fd, int level, int optname) {
            T value;
            socklen_t optlen = sizeof(T);

            int res = ::getsockopt(fd, level, optname, &value, &optlen);
            if (res == -1) {
                throw std::system_error(errno, std::generic_category(), "getsockopt");
            }

            return value;
        }

    }

    Socket::~Socket() noexcept {
        if (m_fd != -1) {
            ::close(m_fd);
        }
    }

    Socket &Socket::operator=(Socket &&rhs) noexcept {
        if (m_fd != -1) {
            ::close(m_fd);
        }

        m_fd     = rhs.m_fd;
        rhs.m_fd = -1;
        return *this;
    }

    Socket Socket::make_raw(int domain, int type) {
        // Atomically creating the socket and setting it as CLOEXEC is
        // supported since Linux 2.6.27.
        int fd = ::socket(domain, type | SOCK_CLOEXEC, 0);
        if (fd == -1) {
            throw std::system_error(errno, std::generic_category(), "Socket::make_raw");
        }

        return Socket(fd);
    }

    void Socket::shutdown(Shutdown how) const {
        int val;
        switch (how) {
            case Shutdown::Read:  val = SHUT_RD;
            case Shutdown::Write: val = SHUT_WR;
            case Shutdown::Both:  val = SHUT_RDWR;
        }

        int res = ::shutdown(m_fd, val);
        if (res == -1) {
            throw std::system_error(errno, std::generic_category(), "Socket::shutdown");
        }
    }

    std::optional<std::chrono::nanoseconds> Socket::get_timeout(int kind) const {
        auto value = getsockopt<struct timeval>(m_fd, SOL_SOCKET, kind);
        if (value.tv_sec == 0 && value.tv_usec == 0) {
            return {};
        } else {
            return std::chrono::nanoseconds{value.tv_usec} + std::chrono::seconds{value.tv_sec};
        }
    }

    std::optional<std::chrono::seconds> Socket::get_linger() const {
        auto linger = getsockopt<struct linger>(m_fd, SOL_SOCKET, SO_LINGER);
        if (linger.l_onoff == 0) {
            return {};
        } else {
            return std::chrono::seconds{linger.l_linger};
        }
    }

    bool Socket::get_reuseaddr() const {
        return getsockopt<int>(m_fd, SOL_SOCKET, SO_REUSEADDR) != 0;
    }

    bool Socket::get_nodelay() const {
        return getsockopt<int>(m_fd, IPPROTO_TCP, TCP_NODELAY) != 0;
    }

    bool Socket::get_quickack() const {
        return getsockopt<int>(m_fd, IPPROTO_TCP, TCP_QUICKACK) != 0;
    }

    bool Socket::get_passcred() const {
        return getsockopt<int>(m_fd, SOL_SOCKET, SO_PASSCRED) != 0;
    }

    int Socket::get_ttl() const {
        return getsockopt<int>(m_fd, IPPROTO_IP, IP_TTL);
    }

    void Socket::set_timeout(int kind, std::optional<std::chrono::nanoseconds> t) const {
        constexpr auto MaxSecs = std::chrono::seconds{std::numeric_limits<time_t>::max()};

        struct timeval tv{};
        if (t) {
            auto secs = std::max(std::chrono::duration_cast<std::chrono::seconds>(*t), MaxSecs);

            tv.tv_sec  = static_cast<time_t>(secs.count());
            tv.tv_usec = static_cast<suseconds_t>((*t - secs).count());
            if (tv.tv_sec == 0 && tv.tv_usec == 0) {
                tv.tv_usec = 1;
            }
        }

        setsockopt(m_fd, SOL_SOCKET, kind, tv);
    }

    void Socket::set_linger(std::optional<std::chrono::seconds> linger) const {
        struct linger value{
                .l_onoff  = linger.has_value(),
                .l_linger = static_cast<int>(linger.value_or(std::chrono::seconds::zero()).count()),
        };
        setsockopt(m_fd, SOL_SOCKET, SO_LINGER, value);
    }

    void Socket::set_reuseaddr(bool flag) const {
        setsockopt<int>(m_fd, SOL_SOCKET, SO_REUSEADDR, flag);
    }

    void Socket::set_nodelay(bool flag) const {
        setsockopt<int>(m_fd, IPPROTO_TCP, TCP_NODELAY, flag);
    }

    void Socket::set_quickack(bool flag) const {
        setsockopt<int>(m_fd, IPPROTO_TCP, TCP_QUICKACK, flag);
    }

    void Socket::set_passcred(bool flag) const {
        setsockopt<int>(m_fd, SOL_SOCKET, SO_PASSCRED, flag);
    }

    void Socket::set_ttl(int value) const {
        setsockopt<int>(m_fd, IPPROTO_IP, IP_TTL, value);
    }

    void Socket::set_nonblocking(bool flag) const {
        int opt = flag;
        int res = ::ioctl(m_fd, FIONBIO, &opt);
        if (res == -1) {
            throw std::system_error(errno, std::generic_category(), "Socket::set_nonblocking");
        }
    }

    void Socket::set_mark(int mark) const {
        setsockopt(m_fd, SOL_SOCKET, SO_MARK, mark);
    }

    void Socket::check_error() const {
        int raw = getsockopt<int>(m_fd, SOL_SOCKET, SO_ERROR);
        if (raw != 0) {
            throw std::system_error(errno, std::generic_category(), "Socket::check_error");
        }
    }

    Task<> Socket::connect(const sockaddr *addr, socklen_t len) const {
        auto &rt      = Runtime::instance();
        auto &reactor = rt.reactor();

        while (true) {
            int res = co_await reactor.connect(m_fd, addr, len);
            if (res == -1) {
                switch (errno) {
                    case EINTR:
                        continue;
                    case EISCONN:
                        co_return;

                    default:
                        throw std::system_error(errno, std::generic_category(), "Socket::connect");
                }
            }

            co_return;
        }
    }

    Task<Socket> Socket::accept(sockaddr *storage, socklen_t *len) const {
        auto &rt      = Runtime::instance();
        auto &reactor = rt.reactor();

        // Only known way to accept socket and atomically set CLOEXEC is
        // accept(4), added in Linux 2.6.28.
        int res;
        while (true) {
            res = co_await reactor.accept(m_fd, storage, len, SOCK_CLOEXEC);
            if (res != -1) {
                break;
            } else if (errno != EINTR) {
                throw std::system_error(errno, std::generic_category(), "Socket::accept");
            }
        }

        co_return Socket(res);
    }

    Task<int> Socket::recv(void *buf, size_t len, int flags) const {
        auto &rt      = Runtime::instance();
        auto &reactor = rt.reactor();

        int res = co_await reactor.recv(m_fd, buf, len, flags);
        if (res < 0) {
            throw std::system_error(errno, std::generic_category(), "Socket::recv");
        }

        co_return res;
    }

    Task<int> Socket::send(const void *buf, size_t len, int flags) const {
        auto &rt      = Runtime::instance();
        auto &reactor = rt.reactor();

        int res = co_await reactor.send(m_fd, buf, len, flags);
        if (res < 0) {
            throw std::system_error(errno, std::generic_category(), "Socket::send");
        }

        co_return res;
    }

}
