#include "usync/net/tcp.hpp"

#include "usync/impl/endian.hpp"

namespace usync::net {

    namespace {

        SocketAddr sockaddr_to_addr(sockaddr_storage *storage, socklen_t len) {
            static_cast<void>(len); // Unused.

            if (storage->ss_family == AF_INET) {
                // assert(len >= sizeof(sockaddr_in));
                auto *addr_in = reinterpret_cast<sockaddr_in*>(storage);
                return {
                    Ipv4Addr::from_bits(addr_in->sin_addr.s_addr),
                    usync::impl::to_big_endian(addr_in->sin_port)
                };
            } else if (storage->ss_family == AF_INET6) {
                // assert(len >= sizeof(sockaddr_in6));
                auto *addr_in6 = reinterpret_cast<sockaddr_in6*>(storage);
                return SocketAddr({
                    Ipv6Addr(std::to_array(addr_in6->sin6_addr.s6_addr)),
                    ::usync::impl::to_big_endian(addr_in6->sin6_port),
                    ::usync::impl::to_big_endian(addr_in6->sin6_flowinfo),
                    ::usync::impl::to_big_endian(addr_in6->sin6_scope_id)
                });
            } else {
                throw std::system_error(EINVAL, std::generic_category(), "TcpBase::local_addr");
            }
        }

        template <class Fn>
        inline auto addr_to_sockaddr(const SocketAddr &addr, Fn fn) {
            if (addr.is_v4()) {
                sockaddr_in raw{};
                raw.sin_family = AF_INET;
                raw.sin_addr   = addr.v4.addr.to_in_addr();
                raw.sin_port   = usync::impl::to_big_endian(addr.v4.port);

                return fn(reinterpret_cast<sockaddr*>(&raw), sizeof(raw));
            } else {
                sockaddr_in6 raw{};
                raw.sin6_family   = AF_INET6;
                raw.sin6_addr     = addr.v6.addr.to_in6_addr();
                raw.sin6_port     = usync::impl::to_big_endian(addr.v6.port);
                raw.sin6_flowinfo = usync::impl::to_big_endian(addr.v6.flowinfo);
                raw.sin6_scope_id = usync::impl::to_big_endian(addr.v6.scope_id);

                return fn(reinterpret_cast<sockaddr*>(&raw), sizeof(raw));
            }
        }

    }

    namespace impl {

        SocketAddr TcpBase::local_addr() const {
            sockaddr_storage addr{};
            socklen_t len = sizeof(addr);

            if (getsockname(m_socket.fd(), reinterpret_cast<sockaddr*>(&addr), &len) == -1) {
                throw std::system_error(errno, std::generic_category(), "TcpBase::local_addr");
            }

            return sockaddr_to_addr(&addr, len);
        }

    }

    SocketAddr TcpStream::peer_addr() const {
        sockaddr_storage addr{};
        socklen_t len = sizeof(addr);

        if (getpeername(m_socket.fd(), reinterpret_cast<sockaddr*>(&addr), &len) == -1) {
            throw std::system_error(errno, std::generic_category(), "TcpBase::local_addr");
        }

        return sockaddr_to_addr(&addr, len);
    }

    Task<TcpStream> TcpStream::connect(const SocketAddr &addr) {
        int domain  = addr.is_v4() ? PF_INET : PF_INET6;
        auto socket = impl::Socket::make_raw(domain, SOCK_STREAM);

        co_await addr_to_sockaddr(addr, [&](sockaddr *sockaddr, socklen_t len) {
            return socket.connect(sockaddr, len);
        });
        co_return TcpStream(std::move(socket));
    }

    Task<size_t> TcpStream::read(void *buf, size_t len) const {
        co_return co_await m_socket.recv(buf, len);
    }

    Task<size_t> TcpStream::write(const void *buf, size_t len) const {
        co_return co_await m_socket.send(buf, len, MSG_NOSIGNAL);
    }

    TcpListener TcpListener::bind(const SocketAddr &addr) {
        int domain  = addr.is_v4() ? PF_INET : PF_INET6;
        auto socket = impl::Socket::make_raw(domain, SOCK_STREAM);
        socket.set_reuseaddr(true);

        // Bind the newly created socket.
        int res = addr_to_sockaddr(addr, [&](sockaddr *sockaddr, socklen_t len) {
            return ::bind(socket.fd(), sockaddr, len);
        });
        if (res == -1) {
            throw std::system_error(errno, std::generic_category(), "TcpListener::bind");
        }

        // Start listening.
        if (::listen(socket.fd(), 128) == -1) {
            throw std::system_error(errno, std::generic_category(), "TcpListener::bind");
        }

        return TcpListener(std::move(socket));
    }

    Task<std::pair<TcpStream, SocketAddr>> TcpListener::accept() const {
        sockaddr_storage addr{};
        socklen_t len = sizeof(addr);

        auto socket   = co_await m_socket.accept(reinterpret_cast<sockaddr*>(&addr), &len);
        auto sockaddr = sockaddr_to_addr(&addr, len);

        co_return {TcpStream(std::move(socket)), sockaddr};
    }

}
