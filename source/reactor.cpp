#include "usync/reactor.hpp"

#include "usync/runtime_params.hpp"

namespace usync {

    namespace {

        constexpr inline unsigned int DefaultIoRingFlags = []() {
            auto flags = 0U;

            // Make the ring respect `RuntimeParams.CqEntries` configuration.
            flags |= IORING_SETUP_CQSIZE;
            // Disable interrupt notification overhead; we don't need it as
            // rings are never shared between threads in our model.
            flags |= IORING_SETUP_COOP_TASKRUN;
            flags |= IORING_SETUP_TASKRUN_FLAG;
            // Inform io_uring that only a single thread submits operations
            // to a ring. This enables internal performance optimizations.
            flags |= IORING_SETUP_SINGLE_ISSUER;
            // Defer work in io_uring to the point where we wait for CQEs
            // in the main loop. This reduces overall syscall latency.
            flags |= IORING_SETUP_DEFER_TASKRUN;

            return flags;
        }();

    }

    Reactor::Reactor(const RuntimeParams &params) {
        io_uring_params ring_params{};
        {
            ring_params.cq_entries = params.CqEntries;
            ring_params.flags      = DefaultIoRingFlags;
        }

        int res = io_uring_queue_init_params(params.SqEntries, &m_ring, &ring_params);
        if (res < 0) {
            impl::throw_io_uring_error(res, "Reactor::Reactor");
        }
    }

    Reactor::~Reactor() noexcept {
        io_uring_queue_exit(&m_ring);
    }

    int Reactor::wait_timeout(__kernel_timespec *ts) noexcept {
        // Prepare a dummy pointer for the function's CQE output.
        // We're ignoring it here since we handle CQEs elsewhere.
        io_uring_cqe *cqe;

        // Submit pending CQEs and wait until we get completions
        // or hit the timeout.
        int res = io_uring_submit_and_wait_timeout(&m_ring, &cqe, 1, ts, nullptr);
        if (res >= 0) {
            return res;
        } else if (res != -ETIME) {
            impl::throw_io_uring_error(res, "Reactor::wait");
        }

        // We hit the timeout.
        return -1;
    }

    void Reactor::process_events() {
        unsigned int cqe_count = 0;
        unsigned head;
        io_uring_cqe *cqe;

        auto *ring = &m_ring;
        io_uring_for_each_cqe(ring, head, cqe) {
            ++cqe_count;

            // Attempt to get the registered continuation.
            auto *continuation = static_cast<Continuation*>(io_uring_cqe_get_data(cqe));
            if (continuation != nullptr) [[likely]] {
                continuation->m_io_result = cqe->res;
                continuation->m_handle.resume();
            }
        }

        // TODO: Once we implement exceptions, revisit this again to
        // make sure advance always gets called even when resume throws.
        io_uring_cq_advance(ring, cqe_count);
    }

    io_uring_sqe *Reactor::get_sqe_entry() {
        auto *ring = &m_ring;
        auto *sqe  = io_uring_get_sqe(ring);

        if (sqe == nullptr) [[unlikely]] {
            io_uring_submit_and_get_events(ring);

            sqe = io_uring_get_sqe(ring);
            if (sqe == nullptr) {
                throw std::runtime_error("send help");
            }
        }

        return sqe;
    }

}
