#include "usync/runtime.hpp"

namespace usync {

    Runtime::Runtime(const RuntimeParams &params)
        : m_reactor(params), m_timer_queue(), m_run_queue() {}

    Runtime &Runtime::instance() {
        if (g_runtime == nullptr) {
            throw std::runtime_error("not inside runtime");
        }

        return *g_runtime;
    }

    void Runtime::process_timers() {
        if (!m_timer_queue.is_empty()) {
            const auto clock = std::chrono::steady_clock::now() + std::chrono::nanoseconds{1};
            m_timer_queue.tick(clock);
        }
    }

    void Runtime::tick(std::coroutine_handle<> root) {
        // If we have pending tasks in the run queue, run those first.
        for (; !m_run_queue.empty(); m_run_queue.pop_front()) {
            m_run_queue.front().resume();
        }

        // Next, resume any coroutines blocked on elapsed timers.
        this->process_timers();

        // Now we need to wait for I/O completions for blocked tasks.
        // We're gonna batch-submit all the I/O operations from tasks
        // and timers in one syscall, and service elapsed timers.
        while (true) {
            // Bail early when the coroutine is finished already.
            if (root.done()) [[unlikely]] {
                return;
            }

            // If no more timers are left, wait indefinitely for I/O.
            if (m_timer_queue.is_empty()) {
                m_reactor.wait();
                break;
            }

            // Otherwise, wait for I/O until we hit the next timer's
            // deadline. If we receive I/O before that, we process it,
            // otherwise we service that timer before retrying.
            auto &entry = m_timer_queue.peek();
            if (m_reactor.wait_until(entry.deadline()) != -1) {
                break;
            } else {
                this->process_timers();
            }
        }

        // Now we resume the tasks blocked on completed I/O operations.
        m_reactor.process_events();
    }

}
