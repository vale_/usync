#include "usync/task/spawn.hpp"

#include "usync/runtime.hpp"

namespace usync::impl {

    void spawn(std::coroutine_handle<> h) {
        auto &rt = Runtime::instance();
        rt.spawn_task(h);
    }

}
