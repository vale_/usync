#include "usync/task/yield_now.hpp"

#include "usync/runtime.hpp"

namespace usync {

    void yield_now::await_suspend(std::coroutine_handle<> h) const noexcept {
        auto &rt = Runtime::instance();
        rt.spawn_task(h);
    }

}
