#include "usync/time/sleep.hpp"

#include "usync/runtime.hpp"

namespace usync::impl {

    Sleep::Sleep(const std::chrono::steady_clock::time_point &deadline) noexcept
        : m_entry(Runtime::instance().timer_entry(deadline)) {}

    void Sleep::await_suspend(std::coroutine_handle<> h) noexcept {
        // If we must suspend, register the awaiter as the continuation.
        m_entry.set_continuation(h);

        // Submit the timer to the runtime to get woken when it fires.
        auto &rt = Runtime::instance();
        rt.submit_timer(m_entry);
    }

}
